var express = require('express');
var router = express.Router();
const puppeteer = require('puppeteer');
const axios = require('axios');

const _2CaptchaInAPI = "https://2captcha.com/in.php?";
const _2CaptchaResAPI = "https://2captcha.com/res.php?"
const _GameLogin = "https://napthe.vn/app/100067/idlogin"
const _NapTheInfo = "https://napthe.vn/api/auth/player_id_login"

function _sleepFor(second) {
    return new Promise((resolve, reject) => {
        setTimeout(resolve, second * 1000)
    })
}

function _request(url, query) {
    return new Promise((resolve, reject) => {
        let queryStr = "";
        for (let param in query) {
            queryStr += param + "=" + query[param] + "&";
        }
        console.log("_______REQUEST", url + queryStr)
        axios.get(url + queryStr).then((response) => {
            console.log(response);
            resolve(response)
        }).catch((err) => reject(err))
    })

}
/* GET home page. */
router.get('/', function(req, res, next) {
    const _API_KEY = req.query.key || "caf50a766c88411e0bb582ebf2b62034";
    const _USER_ID = req.query.userid || "1709131385";
    (async() => {
        try {
            const browser = await puppeteer.launch({ headless: true });
            const page = await browser.newPage();
            await page.goto(_GameLogin);
            await page.waitForSelector("iframe[src*='api2/anchor']");
            const captchaSrc = await page.evaluate(() => {
                return document.querySelector("iframe[src*='api2/anchor']").src;
            })

            const googleKey = captchaSrc.match(/k=(.+?)&/)[1];

            let _res = null;
            //Get balanced
            _res = (await _request(_2CaptchaResAPI, { action: "getbalance", key: _API_KEY, json: 1 })).data;
            if (_res.status !== 1) {
                res.status(500).json({ err: 1, message: "_2CAPTCHA_GET_BALANCE_EXCEPTION" });
                return;
            }
            let _2CaptchaBalance = +_res.request
            if (+_res.request <= 0) {
                res.status(500).json({ err: 1, message: "_2CAPTCHA_RES_OUT_OF_BALANCE" });
                return;
            }
            //Submit captcha 
            _res = (await _request(_2CaptchaInAPI, { pageurl: _GameLogin, googlekey: googleKey, method: "userrecaptcha", key: _API_KEY, json: 1 })).data;
            if (_res.status !== 1) {
                res.status(500).json({ err: 1, message: "_2CAPTCHA_IN_API_EXCEPTION" });
                return;
            }
            const jobId = _res.request;
            let waitRoundLeft = 8;
            let captchaResponse = ""
            while (true) {
                if (--waitRoundLeft < 0)
                    break;
                await _sleepFor(20);
                _res = (await _request(_2CaptchaResAPI, { key: _API_KEY, json: 1, id: jobId, action: "get" })).data;
                if (_res.status !== 1) {
                    continue;
                }
                captchaResponse = _res.request;
                break;
            }
            if (captchaResponse.trim().length === 0) {
                res.status(500).json({ err: 1, message: "_2CAPTCHA_SOLVE_EXCEPTION" });
                return
            }
            axios.post(_NapTheInfo, { app_id: 100067, login_id: _USER_ID, captcha_token: captchaResponse }).then(response => res.status(200).json({ err: 0, data: response.data, balance: _2CaptchaBalance })).catch(err => res.status(500).json({ err: 1, message: "_NAPTHE_GET_INFO_EXCEPTION" }));
        } catch (e) {
            console.log(e)
            res.status(500).json({ err: 1, message: "_PUPPETEER_EXCEPTION" })
        }
    })();

});

module.exports = router;